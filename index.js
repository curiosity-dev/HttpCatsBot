var Discord = require("discord.js");
var snekfetch = require("snekfetch");
var util = require("util");

var bot = new Discord.Client({disableEveryone: true});

var owner = process.env.OWNER;
var prefix = process.env.PREFIX;
var token = process.env.TOKEN;
var dbl_token = process.env.DBL_TOKEN;

bot.on("ready", () => {
    console.log("Connected!");
    console.log("----------");
    console.log("OAuth Link:\n");
    console.log(`https://discordapp.com/oauth2/authorize?client_id=${bot.user.id}&scope=bot&permissions=384064`);
    console.log("\n----------");
    bot.user.setActivity(`out for commands on ${bot.guilds.array().length} servers | ${prefix}help`, {type: "WATCHING"}).catch((DiscordAPIError) => {
        console.error(`[Error] Failed to update presence: ${DiscordAPIError}`);
    });
    bot.channels.get("397521327319416854").send(`I'm ready to rumble!`).catch((DiscordAPIError) => {
        console.error(`[Error] Failed to log startup message to logs channel - ${DiscordAPIError}`);
    });
    snekfetch.post("https://discordbots.org/api/bots/"+bot.user.id+"/stats", {headers: {"Authorization": dbl_token}}).send({server_count: bot.guilds.array().length}).catch(DBLPostError => {
        console.error(`[Error] Failed to post server count to DBL: ${DBLPostError}`);
    });
});

bot.on("message", message => {
    var command = message.content.toLowerCase();
    if (!message.guild.available) return console.warn(`[Warning!] Guild ${message.guild} (${message.guild.id}) is not available, might be under an outage!`);
    if (message.author.bot) return;
    if (message.author === bot.user) return;
    /*if (command === `<@${bot.user.id}>` || `@${bot.user.tag}`)
    {
	message.reply("Hey there! I'm HttpCatsBot, and I can post images of HTTP cats from http.cat for you! Use hc!help to get started!");
    }*/
    if (message.content.startsWith(prefix))
    {
        var cmd_used_msg = `[Command Used] ${message.author.tag} (${message.author.id}) -> ${message.content}`;
        console.log(cmd_used_msg);
        bot.channels.get("397521327319416854").send(cmd_used_msg).catch((DiscordAPIError) => {
            console.error(`[Error] Failed to send command usage log to channel - ${DiscordAPIError}`);
        });
        if (command.startsWith(`${prefix}httpcat`))
        {
            var http_cat = message.content.split(" ")[1];
            var http_cat_em = new Discord.MessageEmbed();
            http_cat_em.setTitle("Here's your HTTP cat!");
            http_cat_em.setColor("#7289DA");
            http_cat_em.setDescription("Cat: "+http_cat);
            http_cat_em.setImage(`https://http.cat/${http_cat}`);
            http_cat_em.setFooter("Powered by the http.cat API");
            message.channel.send(http_cat_em).catch((DiscordAPIError) => {
                message.channel.send(`https://http.cat/${http_cat}`).catch((DiscordAPIError) => {
                    message.channel.send(`:x: I couldn't send the image because I'm missing permissions to do so (\`Embed Links\``);
                });
            });
        }
        else if (command === `${prefix}support`) {
            message.channel.send(`Need support? Join the support server: https://disco.gg/curiositydev`);
        }
        else if (command === `${prefix}help`) {
            var help_embed = new Discord.MessageEmbed();
            help_embed.setTitle("Help");
            help_embed.setColor("#7289DA");
            help_embed.addField(`${prefix}httpcat <http status code>`, "Returns the HTTP Cat image of an HTTP status code. **`<code>` should be replaced by a HTTP status code from [http.cat](https://http.cat) when using this command.**");
            help_embed.addField(`${prefix}support`, "Gives you a link to the support server");
            help_embed.addField(`${prefix}invite`, "Gives you a link to invite me to your server!");
            help_embed.addField(`${prefix}about`, "Gives you information about me!");
            help_embed.addField(`${prefix}help`, "Returns the commands list for me!");
	    help_embed.addField(`${prefix}suggest`, "Gives information and instructions on suggesting features and other stuff for this bot.");
            help_embed.addField(`${prefix}donate`, "Returns information on donating to support my developer and keep me online!");
            if (message.author.id === owner) {
                help_embed.addField(`${prefix}eval <code>`, "Evaluates arbitrary JavaScript code.");
            }
            help_embed.setTimestamp();
            message.channel.send(help_embed).catch((DiscordAPIError) => {
                message.channel.send(":x: I need the `Embed Links` permission to send the help command!");
            });
        }
        else if (command === `${prefix}invite`) {
            message.channel.send(`Here's my invite link! (Note: All permissions asked for are **required**). <https://discordapp.com/oauth2/authorize?client_id=${bot.user.id}&scope=bot&permissions=384064>`);
        }
        else if (command === `${prefix}about`) {
            var about_embed = new Discord.MessageEmbed();
            about_embed.setTitle("About me!");
            about_embed.setColor("#7289DA");
            about_embed.addField(`Developed by`, "Hexexpeck#8781");
            about_embed.addField("Published by", "CuriosityDev");
            about_embed.addField("Language", "JavaScript (Node.js)");
            about_embed.addField("Discord API library", `Discord.js master branch (v${Discord.version})`);
            about_embed.addField("Node.js version", process.version);
            about_embed.addField("GitHub Repository", "[CuriosityDev/HttpCatsBot](https://github.com/CuriosityDev/HttpCatsBot)");
            about_embed.addField("Help command", `\`${prefix}help\``);
            about_embed.addField("Want to help keep me online?", "Then donate to me [by clicking here](https://ko-fi.com/Hexexpeck)");
            about_embed.setFooter("Powered by the HTTP.cat API");
            about_embed.setTimestamp();
            message.channel.send(about_embed).catch((DiscordAPIError) => {
                message.channel.send(":x: I need the `Embed Links` permission to send the about command!");
            });
        }
        else if (command.startsWith(`${prefix}eval`) && message.author.id === owner)
        {
            var suffix = message.content.substring(8);
			if (suffix.toLowerCase().includes("bot.token" || "token" || "process.env.token" || "process.env.TOKEN")) {
				return message.reply(":x: no token 4 u");
            }
            try {
			    message.channel.send(":arrow_down: `INPUT:`\n```js\n" + suffix + "```\n:arrow_up: `OUTPUT:`\n```js\n" + util.inspect(eval(suffix)).replace(bot.token, "(Token)") + "```");
			} catch (e) {
				message.channel.send(":arrow_down: `INPUT:`\n```js\n" + suffix + "```\n:sos: `ERROR:`\n```js\n" + e.stack + "```");
            }
        }
        else if (command === `${prefix}donate`) {
            message.reply("If you wish to donate to me and help keep me online, then you can donate via this link: https://ko-fi.com/Hexexpeck");
        }
	else if (command.startsWith(`${prefix}suggest`)) {
	    message.reply("You can suggest stuff for me over at the Hexexpeck/CuriosityDev Feedback Center! https://suggestions.hexexpeck.me");
	}
    }
});

bot.on("error", err => {
    console.error("[Bot Error] Error in bot: "+err);
});

bot.on("guildCreate", guild => {
    var guildcreate_msg = `[Joined Guild] ${guild.name} (${guild.id}) - now in ${bot.guilds.array().length} guilds`;
    console.log(guildcreate_msg);
    bot.channels.get("397521327319416854").send(guildcreate_msg).catch((DiscordAPIError) => {
        console.error(`[Error] Failed to log guildCreate - ${DiscordAPIError}`);
    });
    bot.user.setActivity(`out for commands on ${bot.guilds.array().length} servers | ${prefix}help`, {type: "WATCHING"}).catch(PresenceError => {
        console.error(`[Error] Failed to update guildCreate presence - ${PresenceError}`);
        bot.channels.get("397521327319416854").send(":x: Failed to update guildCreate presence - "+PresenceError);
    });
});

bot.on("guildDelete", guild => {
    var guilddelete_msg = `[Left Guild] ${guild.name} (${guild.id}) - now in ${bot.guilds.array().length} guilds`;
    console.log(guilddelete_msg);
    bot.channels.get("397521327319416854").send(guilddelete_msg).catch((DiscordAPIError) => {
        console.error(`[Error] Failed to log guildDelete - ${DiscordAPIError}`);
    });
    bot.user.setActivity(`out for commands on ${bot.guilds.array().length} servers | ${prefix}help`, {type: "WATCHING"}).catch(PresenceError => {
        console.error(`[Error] Failed to update guildDelete presence - ${PresenceError}`);
        bot.channels.get("397521327319416854").send(":x: Failed to update guildDelete presence - "+PresenceError);
    });
})

bot.login(token).catch(console.error);

process.on("unhandledRejection", (PromiseRejection) => console.error(`[Promise Error] ${PromiseRejection}`));
