# HttpCatsBot
A Discord bot to get images from https://http.cat

[![Discord Bots](https://discordbots.org/api/widget/397513918085595136.svg)](https://discordbots.org/bot/397513918085595136)

## How to use?
Invite the bot by clicking [here](https://discordapp.com/oauth2/authorize?client_id=397513918085595136&scope=bot&permissions=384064) (all permissions are **required**), then use the `hc!httpcat <code>` to get the image. **Please note that `<code>` should be replaced by a code from https://http.cat to get the image, dont just literally type `<code>`**

### Discord Terms of Service Compliance
By using this bot, you are agreeing to your information being logged. This information includes:

- User IDs of those who use the bot's commands
- Server IDs
- Usernames/discriminators of those who use the bot's commands
- Channel IDs
- Message IDs

If you do not agree to this, do not add the bot. Simple.

## Selfhosting
Sure, under these terms:

You may not:
- Claim the bot instance you're hosting as the official/main one
- Submit your bot instance to any discord bots listing services
- Provide support for your bot instance (direct all support requests to the CuriosityDev server [here](https://disco.gg/curiositydev))
- Edit the code to violate any of these terms

So yeah.

Have fun

### Selfhosting Setup
You'll need to add/edit 3 environment variables to get the bot to work:
`TOKEN`, `OWNER`, and `PREFIX`.

## License
This project is licensed under the [MIT License](https://github.com/CuriosityDev/HttpCatsBot/blob/master/LICENSE)
